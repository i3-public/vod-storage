<?php

#
# disk usage inquiry
#

chdir('/var/www/html');
include_once('/var/www/inc/.php');


#
# prevent web requests
if(! isset($argv) ) die('...');
# 


#
# semaphore
$semaphore = sizeof( curr_proc('cron-avi.php') );
if( $semaphore > 1 ) die();
#





while( 1 ){

	if( $json = fgct( SIGNAL_POINT.'/api/feed/vods/avi_check/pool.php' ) ){

		$avi_check = 0;

		list($id, $vod_cache) = array_values( json_decode($json, true) );
		logg($vod_cache);
		$vod_cache = explode('.i3ns.net:8093/', $vod_cache)[1];

		// logg($id);
		// logg($vod_cache);

		if(! file_exists($vod_cache) ){
			$avi_check = -2;

		} else {

			$res = shell_exec("ffmpeg -i $vod_cache > /tmp/avi.check 2>&1 ; cat /tmp/avi.check | grep Input | grep 'avi,' | wc -l ; rm -rf /tmp/avi.check ");
			$res = intval(trim($res, "\r\n\t "));

			$avi_check = 1;

			if( $res === 1 ){
				
				$avi_check = -1;
				
				$vod_replace = file_with_new_extn($vod_cache, 'avi', 'mp4');
				logg($vod_replace);

				shell_exec("ffmpeg -y -i $vod_cache -vcodec libx264 -acodec aac $vod_replace");
				
				if( file_exists($vod_replace) and filesize($vod_replace) > 100000000 ){
					unlink($vod_cache);
					$avi_check = 2;

				} else {
					unlink($vod_replace);					
				}

			}

		}

		$res = fgct( SIGNAL_POINT."/api/feed/vods/avi_check/save.php?id={$id}&avi_check={$avi_check}" );
		logg("::> $avi_check: $res");

		if( $ik++ >= 60 ){
			logg('done');
			die();

		} else {
			// logg($ik);
			// sleep(1);
		}
	
	} else for( $iw=0; $iw<=60; $iw++ ){
		logg(". ");
		sleep(1);
	}
	
}


function file_extn( $file, $skiplowecase=false ){

	$base = basename($file);
	
	if(! $skiplowecase ){
		$base = strtolower($base);
	}

	if(! strstr($base, '.') ){
		return '';

	} else {
		return substr(strrchr($base, '.'), 1);
	}

}


function file_with_new_extn($file, $curr, $new){
	
	$curr = strtolower($curr);
	$new  = strtolower($new);
	
	if( file_extn($file) == $curr ){
		$file = substr($file, 0, -1*strlen($curr)).$new;
	}

	return $file;

}








