<?php

#
# disk usage inquiry
#

chdir('/var/www/html');
include_once('/var/www/inc/.php');


#
# prevent web requests
if(! isset($argv) ) die('...');
# 


#
# semaphore
$semaphore = sizeof( curr_proc('cron-h264.php') );
if( $semaphore > 1 ) die();
#





while( 1 ){

	if( $json = fgct( SIGNAL_POINT.'/api/feed/vods/h264_check/pool.php' ) ){

		$h264 = 0;

		list($id, $vod_cache) = array_values( json_decode($json, true) );
		logg($vod_cache);
		$vod_cache = explode('.i3ns.net:8093/', $vod_cache)[1];

		logg($id);
		logg($vod_cache);

		if(! file_exists($vod_cache) ){
			$h264 = -2;

		} else {

			$res = shell_exec(" ffmpeg -i $vod_cache > /tmp/h264.test 2>&1 ; cat /tmp/h264.test ; rm -rf /tmp/h264.test ");
			
			foreach( [ 'encoder' ] as $code ){ // 'Lavf58.22.100', 'libmatroska' , 'Lavf57.83.100'
				if( stristr($res, $code) ){
					$h264 = 1;
					break;
				}
			}

			logg($h264);

			if( $h264 == 0 ){
				
				$h264 = -1;

				$vod_replace = dirname($vod_cache) . '/conv-'.basename($vod_cache);
				logg($vod_replace);

				shell_exec(" ffmpeg -y -i $vod_cache -vcodec libx264 -acodec aac $vod_replace ");
				
				if( file_exists($vod_replace) and filesize($vod_replace) > 100000000 ){
					unlink($vod_cache);
					rename($vod_replace, $vod_cache);
					$h264 = 2;

				} else {
					unlink($vod_replace);					
				}

			}

		}

		fgct( SIGNAL_POINT."/api/feed/vods/h264_check/save.php?id={$id}&h264={$h264}" );
		logg("::> ".$h264);

		if( $ik++ >= 10 ){
			logg('die');
			die();

		} else {
			logg($ik);
			sleep(1);
		}
	
	} else for( $iw=0; $iw<=60; $iw++ ){
		logg(". ");
		sleep(1);
	}
	
}




