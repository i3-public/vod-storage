<?php

chdir('/var/www');
include_once('/var/www/inc/.php');

#
# kill the old ps

$ps_s = shell_exec(' ps -eo pid,lstart,cmd | grep -v grep | grep dlmagnet  | awk {\' print $3 " " $4 " " $5 " " $6 " " $1 " " $(NF)  \'} ');
$ps_s = explode(PHP_EOL, $ps_s);

foreach( $ps_s as $ps ){
	if(! $ps = trim($ps, "\r\n\t ") )
		continue;
	list($Mon, $day, $time, $year, $pid, $torrent_id) = explode(" ", $ps);
	$time = strtotime("{$day} {$Mon}. {$year} {$time}");
	$diff = date('U') - $time;

	if( $diff >= TORRENT_TIMEOUT * 60 ){
		// cant flag
		fgct( SIGNAL_POINT.'/api/feed/vods/torrent/sync/?json='.text_compress( json_encode([ $torrent_id, 'cant' ]) ) );
		// kill process
		shell_exec(" sudo kill {$pid} ");
		// remove file
		$dest = trim(shell_exec(" cat '/tmp/webtorrent-log/{$torrent_id}' | head -1 | grep torrent_destination | awk {'print \$2'} "), "\r\n\t ");
		if( strstr($dest, '/storage') ){
			shell_exec(" rm -rf {$dest} ");
		}
		// log
		shell_exec(' echo "'.date('H:i:s').' kill: '.$pid.' -> '.$time.', diff '.floor($diff/3600).' hours, dest '.$dest.'" after '.TORRENT_TIMEOUT.' min >> /tmp/log.torrent ');

	} else {
		// shell_exec(' echo "'.date('H:i:s').' okay: '.$pid.' -> '.$time.', diff '.floor($diff/3600).'" >> /tmp/log.torrent ');
	}

}

# 
# start the new process
for( $i=0; $i<10; $i++ ){

	
	# 
	# count of curr torrent
	$curr = sizeof( curr_proc(['php', 'dlmagnet.php']) );
	#

	// shell_exec('echo "curr: '.$curr.' ; COUNT_OF_TORRENT:'.COUNT_OF_TORRENT.'" >> /tmp/log.torrent');

	#
	if( $curr < COUNT_OF_TORRENT ){

		#
		# feed
		echo SIGNAL_POINT.'/api/feed/vods/torrent/pool/?count='. ( COUNT_OF_TORRENT - $curr )."\n";
		if(! $json = fgct( SIGNAL_POINT.'/api/feed/vods/torrent/pool/?count='. ( COUNT_OF_TORRENT - $curr ) ) ){
			echo "Cant get data from SIGNAL POINT";
			die;
		
		} else {
			echo "Got the signal\n";
		}

		#
		# try to download
		$json = json_decode($json, true);
		if( sizeof($json) ){
			
			foreach( $json as $id => $magnet ){
				shell_exec("echo '".date('H:i:s')." got something to do: ".$id."' >> /tmp/log.torrent");
				$cmd = "php dlmagnet.php \"$magnet\" $id > /dev/null 2>/dev/null &";
				
				echo $cmd."\n";
				shell_exec( $cmd );

			}

		} else {
			echo "no task\n";
		}
		#

	}


	echo ".\n";
	flush();

	break;
	
	// if( isset($_GET['once']) ) break;
	// else sleep(5);

}
