<?php

chdir('/var/www');
include_once('/var/www/inc/.php');


if(! $json = fgct( SIGNAL_POINT.'/api/feed/etc/server_list/' ) ){
	echo "cant connect to xwork\n";

} else {

	$ip_s = json_decode($json, true);

	if(! sizeof($ip_s) ){
		echo "no ip found\n";

	} else if( file_exists('/etc/nginx') ){
		access_for_nginx($ip_s);

	} else {
		access_for_apache($ip_s);
	}

}



function access_for_nginx( $ip_s ){
	
	$file = '/etc/nginx/conf.d/blockips.conf';

	foreach( $ip_s as $section => $list ){
		foreach( $list as $ip_n_port => $name ){
			list($ip, $port) = explode(':', $ip_n_port);
			$bl.= "allow $ip;\n";
		}
	}

	$bl.= "deny all;\n";

	if( !file_exists($file) or file_get_contents($file) != $bl ){
		file_put_contents($file, $bl);
		echo shell_exec("/usr/sbin/nginx -s reload");
		echo "\ndone\n";
	
	} else {
		echo "same\n";
	}

}



function access_for_apache( $ip_s ){
	
	$ht = "Options -Indexes\n";
	$ht.= '<FilesMatch "\.(mkv|mp4|avi|mov|mpg|mpeg)$">'."\n\n";
	$ht.= "\torder deny,allow\n";
	$ht.= "\tdeny from all\n";

	foreach( $ip_s as $section => $list ){

		// $ht.= "\n\t# $section\n";

		foreach( $list as $ip_n_port => $name ){
			list($ip, $port) = explode(':', $ip_n_port);
			$ht.= "\tallow from $ip\n"; // # $name
		}

	}

	$ht.= "\n</FilesMatch>\n";

	file_put_contents('/var/www/html/.htaccess', $ht);
	echo "done\n";

}



