<?php 

include_once('var/www/.php');
chdir('/var/www/html');

# php wget.php $line > /dev/null 2>/dev/null &

#
# vars
$line = $argv[1];
$stgc = sizeof(glob('/var/www/html/storage*'));
$dest = '/storage'.rand(0,$stgc-1).'/'. date('Y/m/d/H/'). basename($line);


#
# prepare the destination
prepare_dest( dirname($dest) );

#
# get the file

echo shell_exec(" wget -U \"96180ef8e8512f7fd17f84ef1f683c88\" $line -O $dest ");

#
# sync with xwork
$path = 'http://'.gethostname().':8093/'.$dest;
$json = json_encode([ $line, $path ]);
$json = text_compress($json);
#

fgct( SIGNAL_POINT.'/api/feed/vods/store/sync/?json='.$json );


