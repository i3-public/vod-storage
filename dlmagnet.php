<?php

chdir('/var/www/html');
include_once('../inc/.php');

#
# php dlmagnet.php $magnet $id > /dev/null 2>/dev/null &
#

#
# vars
$magnet = $argv[1];
$id = $argv[2];
$stgc = sizeof(glob('/var/www/html/storage*'));
$dest = prepare_dest( '/storage' . rand(0,$stgc-1) .'/'. date('Y/m/d/H/') . $id );
#
$log = prepare_dest('/tmp/webtorrent-log/') . $id;
shell_exec("echo \"torrent_destination {$dest}\" > {$log}");
#

#
# get the file
$cmd = "PATH='/root/.nvm/versions/node/v16.14.2/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' && /usr/bin/webtorrent '$magnet' -o $dest --on-done undefinedcommand_$id >> $log 2>&1";
// echo $cmd;die;
echo shell_exec($cmd);
#

#
# check the file, and put the main file in directory, 
# and then address it. (append it to $dest)
// $dest_size = explode(' ', shell_exec("du -s --block-size=1 $dest"))[0];

$list = list_of_video_files($dest);
$list_z = sizeof($list);

# shell_exec('echo "list_z '.$list_z.'" >> ~/log');

if( $list_z <= 0 ){
    shell_exec(" rm -rf $dest ");
    shell_exec(" echo 'cant $id' >> ~/log ");
    fgct( SIGNAL_POINT.'/api/feed/vods/torrent/sync/?json='.text_compress( json_encode([ $id, 'cant' ]) ) );

} else {


    $the_file = ( $list_z == 1 ? $list[0] : ffmpeg_merge_files($list, $dest) );
    shell_exec('echo "the_file '.$the_file.'" >> ~/log');

    $destfile = $dest.strrchr($the_file, '.');
    rename($the_file, $destfile);
    shell_exec("rm -rf $dest");
    shell_exec("chmod 0644 $destfile");
    

    #
    # sync with xwork
    $file = 'http://'.gethostname().':8093'.$destfile;
    $json = text_compress( json_encode([ $id, $file ]) );
    fgct( SIGNAL_POINT.'/api/feed/vods/torrent/sync/?json='.$json );
    # shell_exec("rm -rf $log");
    shell_exec("echo 'DONE $id' >> ~/log");
    #


}



function path_the_biggest_file( $path ){

    $max = 0;
    $max_file = '';

    $dp = opendir($path);

    while( $d = readdir($dp) ){
        
        if( in_array($d, ['.', '..'] ) ) continue;

        $file = $path."/".$d;
        
        if( is_file($file) ){
            if( filesize($file) > $max ){
                $max = filesize($file);
                $max_file = $file;
            }
        
        } else {
            
            list($sub_max, $sub_max_file) = path_the_biggest_file($file);
            
            if( $sub_max > $max ){
                $max = $sub_max;
                $max_file = $sub_max_file;
            }

        }

    }

    closedir($dp);
    return [ $max, $max_file ];

}


function list_of_video_files( $path ){

    $list = [];
    $dp = opendir($path);

    while( $d = readdir($dp) ){
        
        if( in_array($d, ['.', '..'] ) ) continue;

        $file = $path."/".$d;
        
        if( is_file($file) ){
            
            $extn = strtolower( substr( strrchr($file, '.'), 1) );
            
            if( in_array($extn, ['mp4', 'mkv', 'mov', 'mpeg', 'mpg', 'avi', 'flv']) ){
                $list[] = $file;
            }

        } else {
            $list = array_merge( $list, list_of_video_files($file) );
        }

    }
    
    closedir($dp);
    return $list;
    
}


function ffmpeg_merge_files( $list, $dest ){
    
    $out = $dest.'/.out'.strtolower(strrchr($list[0], '.'));
    $txt = $dest.'/.txt';

    shell_exec('echo "" > '.$txt);
    foreach( $list as $file ){
        shell_exec('echo "file \''.$file.'\'" >> '.$txt);
    }
    
    shell_exec("ffmpeg -f concat -safe 0 -i $txt -c copy $out >/dev/null 2>&1");

    return $out;

}



