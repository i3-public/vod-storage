
# 
# wget -qO- https://gitlab.com/i3-public/vod-storage/-/raw/main/README.txt | bash

conf=https://gitlab.com/i3-public/conf/-/raw/main/nginx-php/README.txt
patch=https://gitlab.com/i3-public/vod-storage/-/raw/main/conf/patch

wget -qO- $conf | bash -s 7.4 vods 8093 $patch '-p 8094:443 -v /storage0:/storage0 --hostname '$(hostname)
