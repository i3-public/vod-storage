<?php

#
# v 1.1
# 12 feb 2020
#

function text_compress( $text ){

	$zip = gzcompress($text);
	$zip = base64_encode($zip);
	$zip = str_replace( [ '/', '=' ], [ '-', '_' ] , $zip);
	$zip = rawurlencode($zip);

	return $zip;

}



function text_decompress( $zip ){
	
	$zip = rawurldecode($zip);
	$zip = str_replace( [ '-', '_' ] , [ '/', '=' ], $zip);
	$zip = base64_decode($zip);
	$text = gzuncompress($zip);

	return $text;

}



