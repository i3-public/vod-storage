<?php



function curr_proc( $incl=null, $excl=null, $skip_sh_c=false ){

	$curr_cmd = 'ps aux | grep -v grep';

	if(! $skip_sh_c ){
		$curr_cmd.= " | grep -v 'sh -c'";
	}

	if( $incl ){
		if(! is_array($incl) ){
			$curr_cmd.= " | grep '$incl'";

		} else foreach( $incl as $word ){
			$curr_cmd.= " | grep '$word'";
		}
	}

	if( $excl ){
		if(! is_array($excl) ){
			$curr_cmd.= " | grep -v '$excl'";

		} else foreach( $excl as $word ){
			$curr_cmd.= " | grep -v '$word'";
		}
	}

	$ps_s = shell_exec($curr_cmd);
	$curr = [];

	foreach( explode("\n", $ps_s) as $ps ){
		if( $ps = trim($ps, "\r\n\t ") ){
			$curr[] = $ps;
		}
	}

	return $curr;

}


