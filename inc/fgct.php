<?php

# 17 Oct 2021

function fgct( $url, $timeout=4, $error=false ){
	
	$contx = stream_context_create([

		'http' => [
			'timeout' => intval($timeout),
			'user_agent' => '96180ef8e8512f7fd17f84ef1f683c88',
		],

	    'ssl' => [
	        'verify_peer'=>false,
	        'verify_peer_name'=>false,
	    ],

    ]);

	if( $error ){
		$c = file_get_contents( $url, false, $contx );
	
	} else {
		$c = @file_get_contents( $url, false, $contx );
	}

	$status_line = $http_response_header[0];
	preg_match('{HTTP\/\S*\s(\d{3})}', $status_line, $match);
	$status = $match[1];

	if( $status != 200 ){
		return false;

	} else if( substr($c, 0, 22) == "<br />\n<b>Warning</b>:" ){
		echo $c;
		return false;

	} else {
		return $c;
	}
	
}


